const admin = require("firebase-admin");

console.log(__dirname + "\\service-account.json");

const serviceAccount = require(__dirname + "/service-account.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://app-webasamblea-default-rtdb.firebaseio.com",
});

const { getAuth } = require("firebase-admin/auth");

const uid = "1";

getAuth()
  .createCustomToken(uid)
  .then((customToken) => {
    // Send token back to client
    console.log({ customToken });
  })
  .catch((error) => {
    console.log("Error creating custom token:", error);
  });
